*&---------------------------------------------------------------------*
*& Report  Z_ROLLE_SAP_UPLOAD
*&
*&---------------------------------------------------------------------*
*&
*&
*&---------------------------------------------------------------------*

REPORT z_rolle_sap_upload.

************************************************************************
*    T A B E L L E N
************************************************************************
TYPES: BEGIN OF ty_alv,
  ICON      TYPE icon_d,
  TYPE      TYPE bapiret2-TYPE,
  ID        TYPE bapiret2-ID,
  MESSAGE   TYPE bapiret2-MESSAGE,
END OF ty_alv.

DATA:  lt_file        TYPE filetable,
       lt_return      TYPE TABLE OF bapiret2,
       lt_check       TYPE TABLE OF agr_define,
       lt_alv         TYPE TABLE OF ty_alv,
       lt_fields      TYPE TABLE OF dynpread.

************************************************************************
*   K L A S S E N - D E F I N I T I O N
************************************************************************
DATA: lo_alv            TYPE REF TO cl_salv_table,
      lo_functions      TYPE REF TO cl_salv_functions,
      lo_selections     TYPE REF TO cl_salv_selections,
      lo_header         TYPE REF TO cl_salv_form_layout_grid,
      lo_h_flow         TYPE REF TO cl_salv_form_layout_flow,
      lo_cols           TYPE REF TO cl_salv_columns_table.

************************************************************************
*    G L O B A L E   S T R U K T U R E N
***********************************************************************
DATA: ls_file        TYPE file_table,
      ls_check       TYPE agr_define,
      ls_alv         TYPE ty_alv,
      ls_return      TYPE bapiret2,
      ls_field       TYPE dynpread.

************************************************************************
*    G L O B A L E   F E L D E R
************************************************************************
DATA: lv_rolle_name     TYPE AGR_DEFINE-AGR_NAME,
      lv_path           TYPE string,
      lv_length         TYPE I,
      lv_count          TYPE I,
      lv_count_su       TYPE I,
      lv_count_er       TYPE I,
      lv_line           TYPE I,
      lv_mess_su        TYPE string,
      lv_mess_er        TYPE string,
      lv_percentage     TYPE i,
      lv_perc_txt       type c length 3,
      lv_progress_text  TYPE string,
      lv_out            TYPE string,
      lv_in             TYPE string,
      lv_rc             TYPE i,
      lv_data           TYPE string,
      lv_filename       TYPE FILE_TABLE-FILENAME.

************************************************************************
*    A T   S E L E C T I O N - S C R E E N
************************************************************************
SELECTION-SCREEN BEGIN OF BLOCK frm1 WITH FRAME TITLE TEXT-002.
PARAMETERS:  p_path  TYPE string.
SELECTION-SCREEN END OF BLOCK frm1.


SELECTION-SCREEN BEGIN OF LINE.
  SELECTION-SCREEN COMMENT (80) comm1.
SELECTION-SCREEN END OF LINE.

SELECTION-SCREEN BEGIN OF LINE.
  SELECTION-SCREEN COMMENT (80) comm2.
SELECTION-SCREEN END OF LINE.

SELECTION-SCREEN BEGIN OF LINE.
  SELECTION-SCREEN COMMENT (80) comm3.
SELECTION-SCREEN END OF LINE.

SELECTION-SCREEN BEGIN OF LINE.
  SELECTION-SCREEN COMMENT (80) comm4.
SELECTION-SCREEN END OF LINE.

SELECTION-SCREEN BEGIN OF LINE.
  SELECTION-SCREEN COMMENT (80) comm5.
SELECTION-SCREEN END OF LINE.

*************************************************************************
*    I N I T I A L I Z A T I O N
************************************************************************
INITIALIZATION.
comm1 = 'Es werden einzelne Rolle mit SAP-Endung hochgeladen'.
comm2 = 'Die Rolle in der Datei muss nur Buchstaben, Zahlen und folgende Zeichen'.
comm3 = 'Backslash(/), Doppelpunkt(:), Unterstricht(_) und Puntk(.) enthalten.'.
comm4 = 'Weitere Zeichen werden nicht erlaubt.'.
comm5 = 'Beispiel: ZBKC_SOL21_OLD_COMPARISON.SAP'.

************************************************************************
*    P R O G R A M M S T A R T
************************************************************************

AT SELECTION-SCREEN ON VALUE-REQUEST FOR p_path.

ls_field = 'p_path'.
APPEND ls_field TO lt_fields.

**** AKTUELLE WERT DER PFAD ERMITTELN***********
CALL FUNCTION 'DYNP_VALUES_READ'
EXPORTING
  dyname     = sy-cprog
  dynumb     = sy-dynnr
TABLES
  dynpfields = lt_fields
EXCEPTIONS
  OTHERS     = 1.
IF sy-subrc = 0.
  READ TABLE lt_fields INTO ls_field INDEX 1.
  lv_in =  ls_field-fieldvalue.
ENDIF.

******* VERZEICHNIS AUSWAEHLEN ********************
CALL METHOD cl_gui_frontend_services=>directory_browse
EXPORTING
  window_title         = 'Verzeichnis auswählen'
  initial_folder       = lv_in
CHANGING
  selected_folder      = lv_out
EXCEPTIONS
  cntl_error           = 1
  error_no_gui         = 2
  not_supported_by_gui = 3
  OTHERS               = 4.

IF sy-subrc = 0 OR lv_out IS INITIAL.
  p_path = lv_out.
ELSE.
  p_path =  lv_in.
ENDIF.


  START-OF-SELECTION.
  lv_path = p_path.

**** DATEI AUS DEM ORDNER SELEKTIERN ***************
  CALL METHOD cl_gui_frontend_services=>directory_list_files
EXPORTING
  directory                   = lv_path
  filter                      = '*.sap'
  files_only                  = 'X'
*       directories_only            =
CHANGING
  file_table                  = lt_file
  COUNT                       = lv_count
EXCEPTIONS
  cntl_error                  = 1
  directory_list_files_failed = 2
  wrong_parameter             = 3
  error_no_gui                = 4
  not_supported_by_gui        = 5
  OTHERS                      = 6.

IF sy-subrc <> 0.
  MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
  WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
ENDIF.

******* ANZAHL der Zeile der Tabelle lt_file *******
  lv_line = LINES( lt_file ).

*** ALLE DATEI DES ORDNERS AUSWÄHLEN *******
LOOP AT lt_file INTO ls_file.
  lv_count = lv_count + 1.

  CONCATENATE lv_path '\' INTO lv_data.
  CONCATENATE lv_data ls_file-filename  INTO lv_filename.

  lv_rolle_name = ls_file-filename.

*********** Fortschriftanzeige der Verarbeitung ********
  lv_percentage = ( lv_count * 100 ) / lv_line.
  lv_perc_txt = lv_percentage.
 concatenate lv_perc_txt '% der selektierten Datensätze verarbeitet' into lv_progress_text separated by space.
  CALL FUNCTION 'SAPGUI_PROGRESS_INDICATOR'
  EXPORTING
    percentage = lv_percentage
    TEXT       = lv_progress_text.

**** Rolle Hochladen ***************
      CALL FUNCTION 'PRGN_UPLOAD_AGR'
      EXPORTING
        filename_for_agr        =  lv_filename
        filetype_for_agr        = 'ASC'
      EXCEPTIONS
        file_open_error         = 1
        file_read_error         = 2
        invalid_type            = 3
        no_batch                = 4
        unknown_error           = 5
        not_authorized          = 6
        activity_group_enqueued = 7
        illegal_release         = 8
        no_valid_data           = 9
        action_cancelled        = 10
        OTHERS                  = 11.

      CALL FUNCTION 'PRGN_CHECK_ROLE_EXISTS'
      EXPORTING
       role_name                 = lv_rolle_name
      EXCEPTIONS
        ROLE_DOES_NOT_EXIST       = 1
        OTHERS                    = 2
        .

    IF sy-subrc = 0.
        CONCATENATE 'Rolle' ls_file-filename  'wurde  hochgeladen' INTO lv_mess_su SEPARATED BY space.
        ls_return-ID = 00.
        ls_return-MESSAGE = lv_mess_su.
        ls_return-TYPE = 'I'.
        lv_count_su = lv_count_su + 1.
        MOVE-CORRESPONDING ls_return TO ls_alv.
        ls_alv-ICON = '3'.
     ELSE.
    CONCATENATE 'Rolle' ls_file-filename 'wurde nicht  hochgeladen' INTO lv_mess_er SEPARATED BY space.
        ls_return-ID = 00.
        ls_return-MESSAGE = lv_mess_er.
        ls_return-TYPE = 'E'.
        lv_count_er = lv_count_er + 1.
        MOVE-CORRESPONDING ls_return TO ls_alv.
        ls_alv-ICON = '1'.
        ENDIF.

      APPEND ls_alv TO lt_alv.

ENDLOOP.
***************************** AUSGABE *********************************
CALL METHOD cl_salv_table=>factory
IMPORTING
  r_salv_table = lo_alv
CHANGING
  t_table      = lt_alv.
lo_functions =  lo_alv->get_functions( ).
lo_functions->set_all( ).

lo_cols = lo_alv->get_columns( ).
lo_cols->set_optimize( abap_true ).
lo_cols->set_exception_column( VALUE = 'ICON' ).

***********************************************************************
CREATE OBJECT lo_header.
lo_h_flow = lo_header->create_flow( row = 1  column = 1 ).
lo_h_flow->create_text( TEXT = 'Anzahl gelesene Datensätze: ' ).
lo_h_flow->create_text( TEXT =  lv_line  ).

lo_h_flow = lo_header->create_flow( row = 2  column = 1 ).
lo_h_flow->create_text( TEXT =  'Davon erfolgreiche hochgeladen:' ).
lo_h_flow->create_text( TEXT =  lv_count_su ).

lo_h_flow = lo_header->create_flow( row = 3  column = 1 ).
lo_h_flow->create_text( TEXT =  'Davon nicht hochgeladen: '  ).
lo_h_flow->create_text( TEXT =  lv_count_er ).

lo_alv->set_top_of_list( lo_header ).

***********************************************************************
lo_selections = lo_alv->get_selections( ).
lo_selections->set_selection_mode( if_salv_c_selection_mode=>row_column ).

lo_alv->display( ).
